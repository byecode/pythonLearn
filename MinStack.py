class MinStack:
    def __init__(self):
        self.L=[]
        self.min = []
    def pop(self):
       minx  =  self.L.pop()
       if self.L.count(minx)<=0 and self.min.count(minx)>0:
            self.min.remove(minx)
       return minx
    def push(self,x):
        if not self.min or x< self.min[-1]:
            self.min.append(x)
        self.L.append(x)
    def top(self):
        return self.L[-1]
    def getMin(self):
        return self.min[-1]